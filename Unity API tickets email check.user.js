// ==UserScript==
// @name         Unity API tickets email check
// @version      0.2
// @description  This script runs a check and alert user if email address field is empty while it is created from Unity API
// @author       Richie Gee
// @include      https://projects.mbww.com/browse/*
// @include      https://projects.mbww.com/projects/IPGMBSUP/queues/custom/*/*
// ==/UserScript==

(function() {
    'use strict';
    if (document.getElementById("reporter-val").innerHTML.search('issue_summary_reporter_unity.api@mbww.com') > 0){
        if (document.getElementById("customfield_10401-val") == null && document.getElementsByClassName("sd-user-value").length == 0){
            alert("This Ticket Created from Unity API and User Email is Empty , Please Manually Edit and Include User Email to the Email Address Field")
        }
    }
})();